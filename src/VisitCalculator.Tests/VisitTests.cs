﻿namespace VisitCalculator.Tests
{
    using System;

    using NUnit.Framework;

    [TestFixture]
    public class VisitTests
    {
        [Test]
        public void VisitToString()
        {
            var visit = new Visit { StartTime = DateTime.Today, EndTime = DateTime.Today.AddHours(1) };
            Assert.That(visit.ToString(), Is.EqualTo(string.Format("{0}-{1}", visit.StartTime, visit.EndTime)));
        }
    }
}
