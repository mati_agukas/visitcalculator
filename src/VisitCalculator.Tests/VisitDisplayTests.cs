﻿namespace VisitCalculator.Tests
{
    using System;

    using NUnit.Framework;

    [TestFixture]
    public class VisitDisplayTests
    {
        [Test]
        public void VisitDisplayToString()
        {
            var visit = new VisitDisplay()
                            {
                                StartTime = DateTime.Today,
                                EndTime = DateTime.Today.AddHours(1),
                                VisitorCount = 2
                            };
            Assert.That(
                visit.ToString(),
                Is.EqualTo(string.Format("{0:HH:mm}-{1:HH:mm} -> {2} külastajat", visit.StartTime, visit.EndTime, visit.VisitorCount)));
        }
    }
}
