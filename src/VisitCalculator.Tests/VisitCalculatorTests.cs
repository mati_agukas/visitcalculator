﻿namespace VisitCalculator.Tests
{
    using System;

    using NUnit.Framework;

    [TestFixture]
    public class VisitCalculatorTests
    {
        [Test]
        public void PeriodsWhenMaximumVisitors()
        {
            var reader = new VisitReader(ResourceHelper.GetStream("VisitReaderTestData.txt"));
            var calculator = new VisitCalculator(reader);

            var today = DateTime.Today;
            var expected = new[]
                               {
                                   new VisitDisplay
                                       {
                                           StartTime = today.Add(new TimeSpan(0, 8, 56, 0)),
                                           EndTime = today.Add(new TimeSpan(0, 8, 59, 0)),
                                           VisitorCount = 6
                                       },
                                   new VisitDisplay
                                       {
                                           StartTime = today.Add(new TimeSpan(0, 10, 14, 0)),
                                           EndTime = today.Add(new TimeSpan(0, 10, 32, 0)),
                                           VisitorCount = 6
                                       }
                               };
            Assert.That(calculator.GetPeriodsWhenMaximumVisitors(), Is.EqualTo(expected).Using(new VisitDisplayComparer()));
        }

        [Test]
        public void FourOverlappingPeriods()
        {
            var reader = new VisitReader(ResourceHelper.GetStream("FourOverlappings.txt"));
            var calculator = new VisitCalculator(reader);

            var today = DateTime.Today;
            var expected = new[]
                               {
                                   new VisitDisplay
                                       {
                                           StartTime = today.Add(new TimeSpan(0, 11, 0, 0)),
                                           EndTime = today.Add(new TimeSpan(0, 12, 0, 0)),
                                           VisitorCount = 4
                                       },
                               };
            Assert.That(calculator.GetPeriodsWhenMaximumVisitors(), Is.EqualTo(expected).Using(new VisitDisplayComparer()));
        }

        [Test]
        public void FourZeroLengthPeriods()
        {
            var reader = new VisitReader(ResourceHelper.GetStream("ZeroLengthPeriods.txt"));
            var calculator = new VisitCalculator(reader);

            var today = DateTime.Today;
            var expected = new[]
                               {
                                   new VisitDisplay
                                       {
                                           StartTime = today.Add(new TimeSpan(0, 0, 0, 0)),
                                           EndTime = today.Add(new TimeSpan(0, 5, 0, 0)),
                                           VisitorCount = 0
                                       },
                                   new VisitDisplay
                                       {
                                           StartTime = today.Add(new TimeSpan(0, 5, 0, 0)),
                                           EndTime = today.Add(new TimeSpan(0, 10, 0, 0)),
                                           VisitorCount = 0
                                       },
                                   new VisitDisplay
                                       {
                                           StartTime = today.Add(new TimeSpan(0, 10, 0, 0)),
                                           EndTime = today.Add(new TimeSpan(0, 11, 0, 0)),
                                           VisitorCount = 0
                                       }
                               };
            Assert.That(calculator.GetPeriodsWhenMaximumVisitors(), Is.EqualTo(expected).Using(new VisitDisplayComparer()));
        }
    }
}
