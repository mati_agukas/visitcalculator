﻿namespace VisitCalculator.Tests
{
    using NUnit.Framework;

    [TestFixture]
    public class VisitParserTests
    {
        [Test]
        public void ParseVisitStartAndEnd()
        {
            var d = VisitParser.Parse("12:15", "13:40");
            
            Assert.That(d.StartTime.Hour, Is.EqualTo(12));
            Assert.That(d.StartTime.Minute, Is.EqualTo(15));
            Assert.That(d.EndTime.Hour, Is.EqualTo(13));
            Assert.That(d.EndTime.Minute, Is.EqualTo(40));
        }
    }
}
