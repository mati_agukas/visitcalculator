﻿namespace VisitCalculator.Tests
{
    using System;

    using NUnit.Framework;

    [TestFixture]
    public class VisitToVisitDisplayMapperTests
    {
        [Test]
        public void MapOneVisitToVisitDisplay()
        {
            var today = DateTime.Today;
            var visit = new Visit
                            {
                                StartTime = today.Add(new TimeSpan(0, 10, 0, 0)),
                                EndTime = today.Add(new TimeSpan(0, 11, 0, 0))
                            };
            var visitDisplayListExpected = new[]
                                               {
                                                   new VisitDisplay
                                                       {
                                                           StartTime = visit.StartTime,
                                                           EndTime = visit.EndTime,
                                                           VisitorCount = 1
                                                       }
                                               };
            var visitDisplayList = VisitToVisitDisplayMapper.Map(new[] { visit });
            Assert.That(visitDisplayList, Is.EqualTo(visitDisplayListExpected).Using(new VisitDisplayComparer()));
        }
    }
}
