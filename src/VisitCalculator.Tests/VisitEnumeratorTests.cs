﻿namespace VisitCalculator.Tests
{
    using System;

    using NUnit.Framework;

    [TestFixture]
    public class VisitEnumeratorTests
    {
        [Test]
        public void EnumerateVisits()
        {
            for (var i = 0; i <= 1; i++)
            {
                var enumerator = new VisitEnumerator(ResourceHelper.GetStream("VisitEnumeratorData.txt"));
                enumerator.MoveNext();
                Assert.That(
                    enumerator.Current,
                    Is.EqualTo(new Visit { StartTime = DateTime.Today, EndTime = DateTime.Today.AddHours(1) })
                        .Using(new VisitComparer()),
                    "First visit row");
                enumerator.MoveNext();
                Assert.That(
                    enumerator.Current,
                    Is.EqualTo(
                        new Visit { StartTime = DateTime.Today.AddHours(1), EndTime = DateTime.Today.AddHours(2) })
                        .Using(new VisitComparer()),
                    "Second visit row");
                enumerator.MoveNext();
                Assert.That(
                    enumerator.Current,
                    Is.EqualTo(
                        new Visit { StartTime = DateTime.Today.AddHours(2), EndTime = DateTime.Today.AddHours(3) })
                        .Using(new VisitComparer()),
                    "Third visit row");
                enumerator.Reset(); // Test reset
            }
        }
    }
}
