﻿namespace VisitCalculator.Tests
{
    using System;
    using System.Collections.Generic;

    public class VisitComparer : IEqualityComparer<Visit>
    {
        public bool Equals(Visit x, Visit y)
        {
            return DateTime.Equals(x.StartTime, y.StartTime) && DateTime.Equals(x.EndTime, y.EndTime);
        }

        public int GetHashCode(Visit obj)
        {
            return obj.ToString().GetHashCode();
        }
    }
}
