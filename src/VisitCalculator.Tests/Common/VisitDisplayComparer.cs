﻿namespace VisitCalculator.Tests
{
    using System;
    using System.Collections.Generic;

    public class VisitDisplayComparer : IEqualityComparer<VisitDisplay>
    {
        public bool Equals(VisitDisplay x, VisitDisplay y)
        {
            return DateTime.Equals(x.StartTime, y.StartTime)
                && DateTime.Equals(x.EndTime, y.EndTime)
                && x.VisitorCount == y.VisitorCount;
        }

        public int GetHashCode(VisitDisplay obj)
        {
            return obj.ToString().GetHashCode();
        }
    }
}
