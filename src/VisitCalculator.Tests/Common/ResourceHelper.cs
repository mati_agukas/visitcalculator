﻿namespace VisitCalculator.Tests
{
    using System.IO;
    using System.Reflection;

    public static class ResourceHelper
    {
        public static Stream GetStream(string resourceName)
        {
            var assembly = Assembly.GetCallingAssembly();
            return assembly.GetManifestResourceStream("VisitCalculator.Tests.Resources." + resourceName);
        }
    }
}
