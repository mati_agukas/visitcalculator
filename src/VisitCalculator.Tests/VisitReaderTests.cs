﻿namespace VisitCalculator.Tests
{
    using System.Linq;

    using NUnit.Framework;

    [TestFixture]
    public class VisitReaderTests
    {
        [Test]
        public void VistisCountInLog()
        {
            var reader = new VisitReader(ResourceHelper.GetStream("VisitReaderTestData.txt"));
            Assert.That(reader.Count(), Is.EqualTo(13));
        }

        [Test]
        public void ReadEmptyFile()
        {
            var reader = new VisitReader(ResourceHelper.GetStream("EmptyFile.txt"));
            Assert.That(reader.Count(), Is.EqualTo(0));
        }
    }
}
