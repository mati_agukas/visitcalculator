﻿namespace VisitCalculator
{
    public class VisitDisplay : Visit
    {
        public int VisitorCount { get; set; }

        public override string ToString()
        {
            return string.Format("{0:HH:mm}-{1:HH:mm} -> {2} külastajat", this.StartTime, this.EndTime, this.VisitorCount);
        }
    }
}