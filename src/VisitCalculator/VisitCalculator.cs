﻿namespace VisitCalculator
{
    using System.Collections.Generic;
    using System.Linq;

    public class VisitCalculator
    {
        private readonly IList<VisitDisplay> statistics;

        public VisitCalculator(IEnumerable<Visit> visits)
        {
            this.statistics = VisitToVisitDisplayMapper.Map(visits);
        }

        public IEnumerable<VisitDisplay> GetPeriodsWhenMaximumVisitors()
        {
            var max = this.statistics.Max(s => s.VisitorCount);
            return this.statistics.Where(s => s.VisitorCount == max);
        }
    }
}
