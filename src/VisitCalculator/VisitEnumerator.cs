﻿namespace VisitCalculator
{
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    using Microsoft.VisualBasic.FileIO;

    public class VisitEnumerator : IEnumerator<Visit>
    {
        private readonly Stream stream;

        private readonly Encoding encoding;

        private TextFieldParser parser;

        public VisitEnumerator(Stream stream)
            : this(stream, Encoding.Default)
        {
        }

        public VisitEnumerator(Stream stream, Encoding encoding)
        {
            this.stream = stream;
            this.encoding = encoding;
            this.Reset();
        }

        public Visit Current { get; private set; }

        object IEnumerator.Current
        {
            get
            {
                return this.Current;
            }
        }

        public void Dispose()
        {
            this.parser.Dispose();
        }

        public bool MoveNext()
        {
            lock (this)
            {
                if (this.parser.EndOfData)
                {
                    return false;
                }

                this.Current = VisitParser.Parse(this.parser.ReadFields());
                return true;
            }
        }

        public void Reset()
        {
            this.parser = new TextFieldParser(this.stream, this.encoding) { Delimiters = new[] { ";", "," } };
        }
    }
}