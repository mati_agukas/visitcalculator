﻿namespace VisitCalculator
{
    using System;

    public class Visit
    {
        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public override string ToString()
        {
            return string.Format("{0}-{1}", this.StartTime, this.EndTime);
        }
    }
}
