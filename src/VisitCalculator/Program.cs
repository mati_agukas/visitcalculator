﻿namespace VisitCalculator
{
    using System;
    using System.IO;

    public class Program
    {
        public static void Main(string[] args)
        {
            var reader = new VisitReader(File.OpenRead(args[0]));
            var calculator = new VisitCalculator(reader);

            Console.WriteLine("Perioodid kus oli enim külastajaid:");
            foreach (var period in calculator.GetPeriodsWhenMaximumVisitors())
            {
                Console.WriteLine(period);
            }

            Console.ReadLine();
        }
    }
}
