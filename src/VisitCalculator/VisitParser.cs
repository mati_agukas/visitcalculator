﻿namespace VisitCalculator
{
    using System;
    using System.Globalization;

    public static class VisitParser
    {
        public static Visit Parse(params string[] visitTimes)
        {
            if (visitTimes.Length != 2)
            {
                throw new Exception("Parse error: " + string.Join(";", visitTimes));
            }

            return new Visit
                       {
                           StartTime = DateTime.ParseExact(visitTimes[0], "HH:mm", CultureInfo.InvariantCulture),
                           EndTime = DateTime.ParseExact(visitTimes[1], "HH:mm", CultureInfo.InvariantCulture),
                       };
        }
    }
}