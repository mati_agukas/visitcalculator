namespace VisitCalculator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class VisitToVisitDisplayMapper
    {
        public static IList<VisitDisplay> Map(IEnumerable<Visit> visits)
        {
            var visitDisplayList = new List<VisitDisplay>();
            VisitDisplay lastVisit = null;
            var visitsList = visits.ToArray();
            foreach (var moment in GetEntryMoments(visitsList).OrderBy(m => m).Distinct())
            {
                var count = visitsList.Count(x => x.StartTime <= moment && x.EndTime > moment);

                if (lastVisit != null)
                {
                    lastVisit.EndTime = moment;
                }

                lastVisit = new VisitDisplay { StartTime = moment, VisitorCount = count };
                visitDisplayList.Add(lastVisit);
            }

            if (lastVisit != null)
            {
                lastVisit.EndTime = visitsList.Max(v => v.EndTime);
                if (lastVisit.StartTime == lastVisit.EndTime)
                {
                    visitDisplayList.Remove(lastVisit);
                }
            }

            return visitDisplayList;
        }

        private static IEnumerable<DateTime> GetEntryMoments(IEnumerable<Visit> visits)
        {
            foreach (var visit in visits)
            {
                yield return visit.StartTime;
                yield return visit.EndTime;
            }
        }
    }
}