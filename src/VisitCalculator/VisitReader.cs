﻿namespace VisitCalculator
{
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    public class VisitReader : IEnumerable<Visit>
    {
        private readonly IEnumerator<Visit> enumerator;
 
        public VisitReader(Stream stream, Encoding encoding)
        {
            this.enumerator = new VisitEnumerator(stream, encoding);
        }

        public VisitReader(Stream stream)
        {
            this.enumerator = new VisitEnumerator(stream);
        }

        public IEnumerator<Visit> GetEnumerator()
        {
            return this.enumerator;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
